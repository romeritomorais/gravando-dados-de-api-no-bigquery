```python
#!/usr/bin/env python
# coding: utf-8

###############################
# escrito por Romerito Morais #
# 14/11/2020 02:08:06         #
###############################

# In[7]:


file = open("secrets.txt", "r")
key = file.readline()


# In[8]:


import requests
import pandas
from google.cloud import bigquery

access_key = {'access_key': key}

api_result = requests.get('http://api.aviationstack.com/v1/flights', access_key)
api_response = api_result.json()
data_json = api_response['data']


# In[21]:


df = pandas.json_normalize(data_json)
df.iloc[:,:10]


# In[10]:


df = df[['flight_date','flight_status','departure.airport','departure.timezone',
         'departure.iata','departure.icao',
         'departure.terminal','departure.gate','flight.codeshared.airline_name','flight.codeshared.flight_number']]

df.columns = ['flight_date','flight_status','departure_airport','departure_timezone',
              'departure_iata','departure_icao','departure_terminal',
              'departure_gate','flight_codeshared_airline_name','flight_codeshared_flight_number']
df


# In[11]:


#instanciando cliente
client = bigquery.Client()
table_id = 'BRONZE_LAYER.flights'

#criando esquema
job_config = bigquery.LoadJobConfig(
    schema=[
        bigquery.SchemaField("flight_date", "STRING"),
        bigquery.SchemaField("flight_status", "STRING"),
        bigquery.SchemaField("departure_airport", "STRING"),
        bigquery.SchemaField("departure_timezone", "STRING"),
        bigquery.SchemaField("departure_iata", "STRING"),
        bigquery.SchemaField("departure_icao", "STRING"),
        bigquery.SchemaField("departure_terminal", "STRING"),
        bigquery.SchemaField("departure_gate", "STRING"),
        bigquery.SchemaField("flight_codeshared_airline_name", "STRING"),
        bigquery.SchemaField("flight_codeshared_flight_number", "STRING"),
          ]
)

# passando dados para o job
job = client.load_table_from_dataframe(
    df, table_id, job_config=job_config
)

# Wait for the load job to complete.
job.result()


```

